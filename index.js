/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import AppRouter from './src/Router/AppRouter';

import BottomAppRouter from './src/Project1/BottomAppRouter';

AppRegistry.registerComponent(appName, () => BottomAppRouter);
