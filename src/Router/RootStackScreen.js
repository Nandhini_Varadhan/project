import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomePage from '../Project/HomePage';
import OTP from '../Project/OTP';
import PhoneNo from '../Project/PhoneNo';
import Slider from '../Project/Slider';



const RootStack = createNativeStackNavigator ();

const RootStackScreen = () => (
    <RootStack.Navigator screenOptions ={{headerShown: false}}>
        <RootStack.Screen name ="Slider" component = {Slider}/>
        <RootStack.Screen name ="PhoneNo" component = {PhoneNo}/>
        <RootStack.Screen name ="OTP" component = {OTP}/>
        <RootStack.Screen name ="Home" component = {HomePage}/>
    </RootStack.Navigator>
);

export default RootStackScreen;