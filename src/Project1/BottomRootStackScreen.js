import React from "react";
import { Image } from 'react-native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import Page1 from "./Page1";
import Page2 from "./Page2";
import Page3 from "./Page3";
import Page4 from "./Page4";
import Page5 from "./Page5";

const HomeStack = createNativeStackNavigator();
const BagStack = createNativeStackNavigator();
const FileStack = createNativeStackNavigator();
const BellStack = createNativeStackNavigator();
const AccountStack = createNativeStackNavigator();


const Tab = createMaterialBottomTabNavigator();

const Main = () =>(
    
    <Tab.Navigator
    initialRouteName="Home"
    activeColor="#D04C8D"
    inactiveColor="#ACACAC"
    labeled={false}
    shifting ={true}
    barStyle={{backgroundColor:'#fff'}}
    >
    <Tab.Screen
        name="Home" 
        component={HomeStackScreen}
        options={{
            tabBarColor: '#fff',
            tabBarIcon: ({ color }) => (
                <SimpleLineIcons name ="home" color={color} size={24}/>
            ),
        }}
    />  
    <Tab.Screen
    name="Briefcase"
        component={BagStackScreen}
        options={{
            tabBarColor: '#fff',
            tabBarIcon: ({ color }) => (
                <SimpleLineIcons name ="briefcase" color={color} size={24}/>
            ),
        }}
    />  
    <Tab.Screen
        name="File"
        component={FileStackScreen}
        options={{
            tabBarColor: '#fff',
            tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name ="file-account" color={color} size={24}/>
            ),
        }}
    />
    
    <Tab.Screen
    name="Notification"
        component={BellStackScreen}
        options={{
            tabBarColor: '#fff',
            tabBarIcon: ({ color }) => (
                <FontAwesome5 name ="bell" color={color} size={24}/>
            ),
        }}
    />
    <Tab.Screen
        name="Account"
        component={AccountStackScreen}
        options={{
            tabBarColor: '#fff',
            tabBarIcon: ({ color }) => (
                <Image source={require('../../assets/Account.png')} style={{ borderRadius:20}} color={color} size={24}/>
            ),
        }}
    />  
    </Tab.Navigator>
)
export default Main;

const HomeStackScreen = () => (
    <HomeStack.Navigator screenOptions ={{headerShown: false}}>
        <HomeStack.Screen name ="Home" component={Page1}/>
        </HomeStack.Navigator>
)
const BagStackScreen = () => (
    <BagStack.Navigator screenOptions ={{headerShown: false}}>
        <BagStack.Screen name ="Bag" component={Page2}/>
        </BagStack.Navigator>
)

const FileStackScreen = () => (
    <FileStack.Navigator screenOptions ={{headerShown: false}}>
        <FileStack.Screen name ="File" component={Page3}/>
        </FileStack.Navigator>
)
const BellStackScreen = () => (
    <BellStack.Navigator screenOptions ={{headerShown: false}}>
        <BellStack.Screen name ="Profile" component={Page4}/>
        </BellStack.Navigator>
)
const AccountStackScreen = () => (
    <AccountStack.Navigator screenOptions ={{headerShown: false}}>
        <AccountStack.Screen name ="Bell" component={Page5}/>
        </AccountStack.Navigator>
)



