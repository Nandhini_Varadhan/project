import React from "react";
import {View,Text, StyleSheet,Image,ImageBackground} from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from "react-native-gesture-handler";
 const Page1 =()=>{
     return(
         <ScrollView>
         <SafeAreaView style={{flex:1,backgroundColor:'#E5E5E5'}}>
         <View style={{backgroundColor:'#fff',borderBottomStartRadius: 30,borderBottomEndRadius: 30}}>
         <View style={{flexDirection:'row'}}>
             <Text style={{marginTop: 21,marginStart:31,fontSize: 20,fontWeight: '500',color: '#000',fontStyle: 'normal'}}>Good Morning,{'\n'}Angie </Text>
             <Feather name="search" size={25} color={'#D04C8D'} style={{marginStart: 100,marginTop:26}}/>
             <Feather name="settings" size={23} color={'#D04C8D'} style={{marginStart: 20,marginTop:26}}/>
         </View>
         <Image source={require('../../assets/Saly-1.png')} style={{alignSelf:'center'}}/>
         <View style={{flexDirection:'row',alignSelf:'center'}}>
         <Image source={require('../../assets/Rating1.png')} style={{marginStart: 40}}/>
         <Text style={{color:'#D04C8D',marginTop: -10,marginStart: 16}}>
         <Text style={{fontWeight:'bold'}}>20%</Text>
         </Text>
         <Text style={{color:'#D04C8D',marginStart: -60,marginTop: 4,fontWeight:'400',fontSize: 12}}>Profile Completion</Text>
         </View>
         <LinearGradient colors={['#57508D','#F54BA1']} style={{marginTop: 50,height: 80,width:320,borderRadius: 20,alignSelf:'center', paddingStart: 15,paddingTop:6,bottom: 30}}>
         <View style={{flexDirection: 'row'}}>
         <View style={{flexDirection: 'column'}}>
          <AntDesign name="warning" size={30} color={'#fff'}/>
          <AntDesign name="warning" size={40} color={'#fff'} style={{marginStart:5,marginTop:-12}}/>
          </View>
          <View style={{flexDirection:'column'}}>
          <Text style={{color:'#fff',fontWeight:'500',marginStart:10,marginTop: 10}}>Complete Your Profiling Assessment</Text>
          <Text style={{color:'#fff',fontSize: 12,marginStart:10,marginTop: 2}}>For the best experience on using MyNext,{'\n'}complete all pending assessments.</Text>
          </View>
         </View>
         </LinearGradient>
         </View>
         <View style={{flex: 1}}>
         <View style={{flexDirection:'row',marginStart: 31,marginTop: 36}}>
         <Text style={{color:'#000',fontSize:18,fontWeight: "500",fontStyle:'normal'}}>Assessments</Text>
         <Text style={[styles.highlight,{marginStart: 140}]}>view all</Text>
         </View>
         <View style={{marginTop:30,bottom: 10}}>
         <ImageBackground source={require('../../assets/1.png')} style={{alignSelf:'center',height: 200,width:320}} imageStyle={{ borderRadius: 20}}>
         <Text style={{marginTop: 110,marginStart:20,color:'#fff',fontWeight:'600',fontSize: 18}}>About Me</Text>
         <Text style={{marginStart:20,color:'#fff',fontWeight: '500',fontSize:11}}>View and Manage information {'\n'}about yourself</Text>
         <View style={{flexDirection:'row'}}>
         <AntDesign name="checkcircle" color={'#00FF38'} size={20} style={{marginStart:12,marginTop:5}}/>
         <Text style={{marginTop: 7,marginStart: 6,fontSize: 12, fontWeight:'500',color:'#00FF38'}}>100% Completed</Text>
         </View>
         </ImageBackground>
         </View>
         <View style={{marginTop:20,bottom: 10}}>
         <ImageBackground source={require('../../assets/2.png')} style={{alignSelf:'center',height: 200,width:320}} imageStyle={{ borderRadius: 20}}>
         <Text style={{marginTop: 110,marginStart:20,color:'#fff',fontWeight:'600',fontSize: 18}}>Catalyst Profiling</Text>
         <Text style={{marginStart:20,color:'#fff',fontWeight: '500',fontSize:11}}>Understand yourself better to help {'\n'}become your own agents</Text>
         <View style={{flexDirection:'row'}}>
         <Image source={require('../../assets/Rating.png')} style={{height:5,width: 150,marginStart: 18,marginTop:14}}/>
         <Text style={{marginTop: 7,marginStart: 12,fontSize: 12, fontWeight:'500',color:'#00FF38'}}>50% Completed</Text>
         <Image source={require('../../assets/Rating2.2.png')} style={{marginStart: -250,height: 5,width:75,marginTop:14}}/>
         </View>
         </ImageBackground>
         </View>
         <View style={{marginTop:20,bottom: 10}}>
         <ImageBackground source={require('../../assets/3.png')} style={{alignSelf:'center',height: 200,width:320}} imageStyle={{ borderRadius: 20}}>
         <Text style={{marginTop: 110,marginStart:20,color:'#fff',fontWeight:'600',fontSize: 18}}>Career Explorations</Text>
         <Text style={{marginStart:20,color:'#fff',fontWeight: '500',fontSize:11}}>Explore potential careers related to {'\n'}your profiling</Text>
         <View style={{flexDirection:'row'}}>
         <Image source={require('../../assets/Rating.png')} style={{height:5,width: 150,marginStart: 18,marginTop:14}}/>
         <Text style={{marginTop: 7,marginStart: 12,fontSize: 12, fontWeight:'500',color:'#FF0000'}}>0% Completed</Text>
         </View>
         </ImageBackground>
         </View>
         <View style={{marginTop:20,bottom: 10}}>
         <ImageBackground source={require('../../assets/4.png')} style={{alignSelf:'center',height: 200,width:320}} imageStyle={{ borderRadius: 20}}>
         <Text style={{marginTop: 110,marginStart:20,color:'#fff',fontWeight:'600',fontSize: 18}}>Employability Factors</Text>
         <Text style={{marginStart:20,color:'#fff',fontWeight: '500',fontSize:11}}>Create self-reflection on your available {'\n'}knowledge, skills and abilities.</Text>
         <View style={{flexDirection:'row'}}>
         <Image source={require('../../assets/Rating.png')} style={{height:5,width: 150,marginStart: 18,marginTop:14}}/>
         <Text style={{marginTop: 7,marginStart: 12,fontSize: 12, fontWeight:'500',color:'#FF0000'}}>0% Completed</Text>
         </View>
         </ImageBackground>
         </View>
         <View style={{marginTop:20,bottom: 10}}>
         <ImageBackground source={require('../../assets/5.png')} style={{alignSelf:'center',height: 200,width:320}} imageStyle={{ borderRadius: 20}}>
         <Text style={{marginTop: 110,marginStart:20,color:'#fff',fontWeight:'600',fontSize: 18}}>English Proficiency</Text>
         <Text style={{marginStart:20,color:'#fff',fontWeight: '500',fontSize:11}}>The online test gives a good indication {'\n'}of your proficiency level.</Text>
         <View style={{flexDirection:'row'}}>
          <Image source={require('../../assets/Rating.png')} style={{height:5,width: 150,marginStart: 18,marginTop:14}}/>
         <Text style={{marginTop: 7,marginStart: 12,fontSize: 12, fontWeight:'500',color:'#FF0000'}}>0% Completed</Text>
         </View>
         </ImageBackground>
         </View>
         <View style={{marginTop:20,bottom: 10}}>
         <ImageBackground source={require('../../assets/6.png')} style={{alignSelf:'center',height: 200,width:320}} imageStyle={{ borderRadius: 20}}>
         <Text style={{marginTop: 110,marginStart:20,color:'#fff',fontWeight:'600',fontSize: 18}}>Basic IT Skills</Text>
         <Text style={{marginStart:20,color:'#fff',fontWeight: '500',fontSize:11}}>This online test is intended to measure {'\n'}your basic knowledge and skils.</Text>
         <View style={{flexDirection:'row'}}>
          <Image source={require('../../assets/Rating.png')} style={{height:5,width: 150,marginStart: 18,marginTop:14}}/>
         <Text style={{marginTop: 7,marginStart: 12,fontSize: 12, fontWeight:'500',color:'#FF0000'}}>0% Completed</Text>
         </View>
         </ImageBackground>
         </View>
         <Text style={{marginTop: 10,borderColor:'#D8D6D6',borderBottomWidth:3,width: 320,alignSelf:'center'}}/>
         <View style={{flexDirection:'row',marginTop: 30}}>
         <Text style={{color:'#000',fontSize: 20,fontWeight: '500',fontStyle:'normal',marginStart:31}}>Blogs & Updates</Text>
         <Text style={{color:'#D04C8D',textDecorationLine:'underline',fontSize: 16,fontWeight: '500',fontStyle:'normal',marginStart: 110}}>view all</Text>
         </View>
         <View style={{flexDirection:'row'}}>
         <View style={{flexDirection:'column',height:240,width:250,marginStart:30,marginTop: 20,borderRadius: 30,backgroundColor:'#fff'}}>
         <Image source={require('../../assets/blog.png')} style={{height: 140,width:250,borderTopLeftRadius: 30,borderTopRightRadius:30}}/>
         <Text style={{marginStart: 10,marginTop: 6,fontSize: 18,fontWeight:'600',fontStyle:'normal',color:'#D04C8D'}}>#Door2Work</Text>
         <Text style={{fontSize: 10,marginStart:10,color:'#000'}}>Our main focus is to empower our fellow {'\n'}Malaysians by providing them with opportunities {'\n'}that enable both career and personal growth.</Text>
         </View>
         <View style={{flexDirection:'column',height:240,width:150,marginStart:20,marginTop: 20,borderRadius: 30,backgroundColor:'#fff'}}>
         <Image source={require('../../assets/Rectangle.png')} style={{height: 140,width:100,borderTopLeftRadius: 30}}/>
         <Text style={{marginStart: 16,marginTop: 6,fontSize: 18,fontWeight:'600',fontStyle:'normal',color:'#D04C8D'}}>Wom</Text>
         <Text style={{fontSize: 12,marginStart:16,color:'#000'}}>Providing {'\n'}stone to  {'\n'}labour markets</Text>
         </View>
         </View>
         <View>
         <Image source={require('../../assets/Logo.png')} style={{marginStart: 30, marginTop: 70}}/>
         <Text style={{marginTop: 10,borderColor:'#D8D6D6',borderTopWidth: 3,width: 320,alignSelf:'center'}}/>
         <Text style={{marginStart: 30,color:'#9A9A9A',fontSize: 12,fontWeight:'500'}}>Locate us:6th Floor, Surian Tower, 1, Jalan PJU 7/3, Mutiara Damansara,{'\n'}47810 Petaling Jaya, Selangor</Text>
         <Text style={{marginStart:30,marginTop: 11,color:'#9A9A9A',fontSize: 12,fontWeight:'500',marginBottom: 30}}>Contact Us    +603 7839 7000</Text>
         </View>
         </View>
         </SafeAreaView>
         </ScrollView>
     )
 }
 export default Page1;
const styles = StyleSheet.create({
 highlight:{
  color:'#D04C8D',
  textDecorationLine:'underline',
  fontSize: 16,
  fontWeight: '500',
  fontStyle:'normal',
  
},
});