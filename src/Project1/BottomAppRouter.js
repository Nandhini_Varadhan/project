import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import BottomRootStackScreen from './BottomRootStackScreen';

const stack = createNativeStackNavigator();

const BottomAppRouter = () => {

    return(

        <NavigationContainer>
            <BottomRootStackScreen/>
        </NavigationContainer>
    );
};

export default BottomAppRouter;