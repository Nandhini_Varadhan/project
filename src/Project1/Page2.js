import React from "react";
import { SafeAreaView,View,Image,Text ,ImageBackground,TextInput,StyleSheet,ScrollView} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import AntDesign from 'react-native-vector-icons/AntDesign';

const Page2 = () =>{
return(
    <ScrollView>
    <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
    <View>
    <ImageBackground source={require('../../assets/Pic2.png')} style={{alignSelf:'stretch',height:250}} >
    <View style={{marginTop: 30,flexDirection:'row'}}>
    <AntDesign name="left" size={20} color={'#fff'} style={{marginStart: 20}}/>
    <Text style={{marginStart: 10,color:'#fff',fontSize: 18,fontWeight:'500',marginTop:-2}}>Back</Text>
    </View>
    </ImageBackground>
    <Text style={{marginStart: 31,marginTop: 30,fontWeight: '500',color:'#000',fontSize: 24}}>Tell us more about yourself</Text>
    <Text style={{color:'#858585',marginStart: 31,fontSize: 14,fontWeight:'500'}}>We will need some basic information about you {'\n'}before we get started.</Text>
    <Image style={{marginTop: 24, alignSelf:'center'}} source={require('../../assets/Pic3.png')} />
    <Text style={[styles.TextDesign,{marginTop:30}]}>Who is your current employer (please {'\n'}provide exact company name)?</Text>
    <TextInput style={styles.InputDesign} placeholder="Max 100 Characters"/>
    <Text style={[styles.TextDesign,{marginTop:25}]}>Sector/Industry</Text>
    <TextInput style={styles.InputDesign} placeholder="Select"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Working Duration (Tenure in Years)</Text>
    <View style={{flexDirection:'row'}}>
    <TextInput style={[styles.Year,{ marginStart:31}]} placeholder="Years"/>
    <TextInput style={[styles.Year,{ marginStart:8}]} placeholder="Months"/>
    </View>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Position/Job title</Text>
    <TextInput style={styles.InputDesign} placeholder="Limit upto 100 characters"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Salary (RM per month before tax)</Text>
    <TextInput style={styles.InputDesign} placeholder="Salary (RM per month before tax)"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Highest Academic Qualification</Text>
    <TextInput style={styles.InputDesign} placeholder="Select"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Highest Institution Country</Text>
    <View style={{flexDirection: 'row',marginTop: 15}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>Malaysia</Text>
    <Text style={[styles.Radio,{marginStart: 47}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>Other Countries</Text>
    </View>
    <TextInput style={styles.InputDesign}placeholder="Name Of Institution"/>
    <TextInput style={styles.InputDesign} placeholder="If Other Countries, select country"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Scope of study</Text>
    <TextInput style={styles.InputDesign} placeholder="Salary (RM per month before tax)"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>Please indicate your current grade</Text>
    <View style={{flexDirection: 'row',marginTop: 15}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>CGPA</Text>
    <Text style={[styles.Radio,{marginStart: 36}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>Grades</Text>
    <Text style={[styles.Radio,{marginStart: 36}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>Others</Text>
    </View>
    <TextInput style={styles.InputDesign} placeholder="Please enter"/>
    <Text style={[styles.TextDesign,{marginTop: 25}]}>English Equivalent Tests</Text>
    <View style={{flexDirection:'row',marginTop: 15}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>MUET</Text>
    </View>
    <View style={{flexDirection:'row',marginTop: 12}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>CEFR</Text>
    </View>
    <View style={{flexDirection:'row',marginTop: 12}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>TOEFL</Text>
    </View>
    <View style={{flexDirection:'row',marginTop: 12}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>IELTS</Text>
    </View>
    <View style={{flexDirection:'row',marginTop: 12}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>Others</Text>
    </View>
    <View style={{flexDirection:'row',marginTop: 12}}>
    <Text style={[styles.Radio,{marginStart: 31}]}/>
    <Text style={[styles.RadioText,{marginTop: -2}]}>I have not taken any test</Text>
    </View>
    <TextInput style={styles.InputDesign} placeholder="Please enter"/>
    <LinearGradient colors={['#F54BA1','#57508D']} style={styles.buttonDesign}>
    <Text style={{alignSelf:'center',color:'#fff',marginTop:20,fontSize: 16,fontWeight:'600'}}>Continue</Text>
    </LinearGradient>
    <Text style={styles.highlight}>Save and Exit</Text>
    </View>
    </SafeAreaView>
    </ScrollView>
)
}
export default Page2;
const styles= StyleSheet.create({
TextDesign:{
    marginStart: 31,
    color:'#000',
    fontSize: 18,
    fontWeight:'500',
},    
InputDesign:{
    marginTop: 20,
    marginStart:30,
    height:60,
    width: 300,
    backgroundColor:'#F3F3F3',
    borderRadius:30,
    padding: 15,
    fontSize: 14,
    fontWeight:'500',
    paddingLeft: 30, 
},
Year:{
    marginTop: 20,
    height:60,
    width: 150,
    backgroundColor:'#F3F3F3',
    borderRadius:30,
    padding: 15,
    fontSize: 14,
    fontWeight:'500',
    paddingLeft: 30,
},
Radio:{
   height:20,
   width:20,
   borderRadius:10,
   borderColor:'#D04C8D',
   borderWidth: 1,
},
RadioText:{
  marginStart: 10,
  fontSize:16,
  fontWeight:'500',
  color:'#000',
},
buttonDesign:{
  marginTop: 40,
  height:60,
  width: 165,
  borderRadius:30,
  alignSelf:'center',
},
 highlight:{
  marginTop: 14,
  color:'#504F8C',
  textDecorationLine:'underline',
  fontSize: 16,
  fontWeight: '500',
  fontStyle:'normal',
  alignSelf:'center',
  marginBottom: 40,
},
});