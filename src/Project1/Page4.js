import React from "react";
import {Text, StyleSheet} from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
 const Page4 =()=>{
     return(
         <SafeAreaView style={styles.container}>
             <Text style={styles.textDesign}>Notification</Text>
         </SafeAreaView>
     )
 }
 export default Page4;

 const styles = StyleSheet.create({
     container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
     },
     textDesign:{
         fontSize: 30,
         fontWeight: 'bold',
         color:'#000',
     }
 })